# Technical Assessment

	
	
1. The automation script is located in the path src/pages & src/tests. It utilises the Testng, hence it follows the below:

	a) The pages package (src/pages) contains all of the web elements and the methods to operate the web elements on the website
	
	b) The test page (src/tests) contains the verification methods
	
	c) To run  use testng 
